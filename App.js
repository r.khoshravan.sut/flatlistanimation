import React, { useState, useEffect } from 'react';
import { SafeAreaProvider, SafeAreaView, } from 'react-native-safe-area-context';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import stringsoflanguages from './src/Components/stringsoflanguages'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Provider } from 'react-redux';
import store from './src/Redux/index';

import CustomDrawerContent from './src/Components/CustomDrawerContent';
import MyStack from './src/MyStack/index'
const Drawer = createDrawerNavigator();
const App = (props) => {
  const lam = async () => {
    const lang = await AsyncStorage.getItem('LanguageId')
    switch (lang) {
      case '1':
        stringsoflanguages.setLanguage('en')
        break;
      case '2':
        stringsoflanguages.setLanguage('fa')
        break;
      default:
        break;
    }
  }
  useEffect(() => {
    // await Orientation.lockToPortrait();
    lam()

  }, [])
  return (
    <Provider store={store}>
      <SafeAreaProvider >
        <NavigationContainer>
          <Drawer.Navigator
            drawerStyle={{
              backgroundColor: '#FFFF',
              overflow: 'hidden',
            }}
            drawerContent={(props) => <CustomDrawerContent {...props} />}>
            <Drawer.Screen name="MyStack" component={MyStack} />
          </Drawer.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </Provider >
  );
}

export default App
