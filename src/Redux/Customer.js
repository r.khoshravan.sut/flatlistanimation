const initialState = {
  LanguageId: '1',
  LanguageArry: [
    {
      LanguageId: '1',
      longform: 'English ',
      shortform: 'en',
      // img: require('../../assets/img/english.png'),
    },
    {
      LanguageId: '2',
      longform: 'فارسی',
      shortform: 'fa',
      // img: require('../../assets/img/iran.png'),
    },

  ],
  userToken: '',
};
const Customer = (state = initialState, action) => {
  // console.log(action)
  switch (action.type) {
    case 'CHANGE_C_userToken':
      return { ...state, userToken: action.userToken };
    case 'CHANGE_C_LanguageId':
      return { ...state, LanguageId: action.LanguageId };
    case 'CHANGE_C_LanguageArry':
      return { ...state, LanguageArry: action.LanguageArry };
    default:
      return state;
  }
};

export default Customer;
