import React, { useState } from 'react';
import { View, Text, Image, KeyboardAvoidingView, StatusBar, } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import stringsoflanguages from '../../Components/stringsoflanguages'
import Header from '../../Components/Header';
import styles from './styles'
import StepOne from './StepOne';
import StepTwo from './StepTwo';
import StepThree from './StepThree';
import SetLanguage from './SetLanguage';
import Helpers from '../../Share/Helper';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Login = (props) => {
    const insets = useSafeAreaInsets();
    const [MyStep, setMyStep] = useState(1)
    const hendlerSteps = () => {
        switch (MyStep) {
            case 1:
                return <StepOne navigation={props.navigation} setMyStep={setMyStep} />
            case 2:
                return <StepTwo navigation={props.navigation} setMyStep={setMyStep} />
            case 3:
                return <StepThree navigation={props.navigation} setMyStep={setMyStep} />
            case 4:
                return <SetLanguage navigation={props.navigation} setMyStep={setMyStep} />
            default:
                return <StepOne navigation={props.navigation} setMyStep={setMyStep} />
        }
    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <StatusBar barStyle="light-content" backgroundColor={Helpers.ReturnStatusColor('blueText')} />
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior="height"
            // keyboardVerticalOffset={100}
            >
                <View style={{
                    paddingTop: insets.top,
                    paddingBottom: insets.bottom,
                    // backgroundColor: 'green',
                    paddingHorizontal: '7%',
                    flex: 1,

                }}>
                    <Header
                        Left={() => {
                            return (
                                <TouchableOpacity
                                    style={{ justifyContent: 'center' }}
                                    onPress={() => props.navigation.toggleDrawer()}>
                                    <Icon name="menu" size={35} style={{ textAlign: 'left' }} />
                                </TouchableOpacity>
                            )
                        }}
                    />
                    <Image
                        resizeMode={'contain'}
                        style={{ width: 45, height: 45, marginHorizontal: '3%', marginVertical: '15%' }}
                        source={require('../../Assets/Icons/Logo.png')}></Image>
                    <View style={styles.viewWelcomtxt}>
                        <Text style={styles.textWelcom}> {stringsoflanguages.welcom} </Text>
                        {
                            stringsoflanguages.the != '' &&
                            <Text style={styles.textWelcom}> {stringsoflanguages.the} </Text>
                        }
                        <Text style={styles.textWelcom}> {stringsoflanguages.CTDev} </Text>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.textWelcom2}> {stringsoflanguages.Newleveloffeatures} </Text>
                            <Text style={styles.textWelcom2}> {stringsoflanguages.Withnewapp} </Text>
                        </View>
                    </View>
                    <View style={styles.bottomView}>
                        {hendlerSteps()}
                    </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Login)