import React, { useState } from 'react'
import { View, Text, TouchableOpacity, TextInput } from 'react-native'
import { connect } from 'react-redux'
import stringsoflanguages from '../../Components/stringsoflanguages'
import styles from './styles'
const StepTwo = (props) => {
    const [txt, settxt] = useState('')
    return (
        <View style={styles.botomViewinner}>
            <TextInput
                value={txt}
                onChangeText={(e) => settxt(e)}
                placeholder={stringsoflanguages.Enteremailorphone}
                placeholderTextColor={'#FFFF'}
                numberOfLines={1}
                style={styles.greybuttomP2} />
            <TouchableOpacity
                onPress={() => props.setMyStep(3)}
                style={styles.redbuttomP2}>
                <Text style={[styles.textWithe, styles.loginTxt]}>{stringsoflanguages.NextStep}</Text>
            </TouchableOpacity>

        </View>
    )

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(StepTwo)