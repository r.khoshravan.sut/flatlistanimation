import React, { useState } from 'react'
import { View, Text, TouchableOpacity, TextInput } from 'react-native'
import { connect } from 'react-redux'
import styles from './styles'
import Icon from 'react-native-vector-icons/Entypo';
import stringsoflanguages from '../../Components/stringsoflanguages'
const StepThree = (props) => {
    const [txt, settxt] = useState('')
    const loginPage = async () => {
        await props.ChengToken('ss')
        props.navigation.replace('MyApp')
    }
    return (
        <View style={styles.botomViewinner}>
            <View style={styles.greybuttomP3}>

                <TextInput
                    value={txt}
                    onChangeText={(e) => settxt(e)}
                    placeholder={stringsoflanguages.EnterCodewehaveSent}
                    placeholderTextColor={'#FFFF'}
                    numberOfLines={1}
                    style={styles.greybuttomP2input} />
                <Icon name="ccw" color="#FFFF" size={25} />
            </View>
            <TouchableOpacity
                onPress={() => loginPage()}
                style={styles.redbuttomP2}>
                <Text style={[styles.textWithe, styles.loginTxt]}>{stringsoflanguages.NextStep}</Text>
            </TouchableOpacity>

        </View>
    )

}

const mapStateToProps = (state) => ({
    userToken: state.Customer.userToken
})

const mapDispatchToProps = (dispatch) => {
    return {
        ChengToken: (userToken) => {
            const action = {
                type: 'CHANGE_C_userToken',
                userToken,
            };
            dispatch(action);
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepThree)