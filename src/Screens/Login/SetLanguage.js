import React from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    StyleSheet,
    Dimensions,
    I18nManager,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage'
import RNRestart from 'react-native-restart';
import styles from './styles'
import { connect } from 'react-redux';
import stringsoflanguages from '../../Components/stringsoflanguages';
// import styles from './styles';

const { width, height } = Dimensions.get('screen');
const SetLanguage = (props) => {
    // const [lang, setlang] = useState([
    //   {
    //     LanguageId: '1', longform: "English ", shortform: 'en',
    //     img: require('../../assets/img/english.png'),
    //   },
    //   {
    //     LanguageId: '5', longform: "فارسی", shortform: 'fa',
    //     img: require('../../assets/img/iran.png'),
    //   },
    //   {
    //     LanguageId: '2', longform: 'Arabic', shortform: 'ar',
    //     img: require('../../assets/img/english.png'),
    //   },]);
    const changeAppLang = async (lang) => {
        if (lang == 'fa') {
            if (!I18nManager.isRTL) {
                await I18nManager.forceRTL(true);
            }
        } else {
            if (I18nManager.isRTL) {
                await I18nManager.forceRTL(false);
            }
        }
        // stringsoflanguages.setLanguage(value);
        RNRestart.Restart();
    };

    global.lang = props.LanguageArry;
    const settext = async (value, LanguageId) => {
        stringsoflanguages.setLanguage(value);
        // console.log('my lang : ' + value)
        props.chengLanguageId(LanguageId);
        await AsyncStorage.setItem('LanguageId', LanguageId);
        // props.nextPage();
        changeAppLang(value);
    };
    return (
        <View style={styles.botomViewinner}>
            <View style={styles.mainviewScrool}>
                <ScrollView style={{ marginTop: 30, width: '85%' }}>
                    {global.lang.map((item, key) => (
                        <TouchableOpacity
                            style={styles.elementContainer}
                            onPress={() => settext(item.shortform, item.LanguageId)}
                            key={key}>
                            <Text
                                // ref={item.shortform}
                                style={[styles.text, { fontSize: 15 }]}>
                                {item.longform}
                            </Text>
                            <Image style={styles.tinyLogo} source={item.img} />
                        </TouchableOpacity>
                    ))}
                    <View style={{ height: 20 }} />
                </ScrollView>
            </View>
        </View>
    );
};

const mapStateToProps = (state) => {
    return {
        LanguageArry: state.Customer.LanguageArry,
        LanguageId: state.Customer.LanguageId,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        chengLanguageId: (LanguageId) => {
            const action = {
                type: 'CHANGE_C_LanguageId',
                LanguageId,
            };
            dispatch(action);
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(SetLanguage);
