import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import styles from './styles'
import stringsoflanguages from '../../Components/stringsoflanguages'

const StepOne = (props) => {

    return (
        <View style={styles.botomViewinner}>
            <View style={styles.botomViewinnerRow}>
                <TouchableOpacity
                    onPress={() => props.setMyStep(2)}
                    style={styles.redbuttom}>
                    <Text style={[styles.textWithe, styles.loginTxt]}>{stringsoflanguages.Login}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => props.setMyStep(4)}
                    style={styles.greybuttomEN}>
                    <Text style={[styles.textWithe, styles.loginTxt]}>en</Text>
                </TouchableOpacity>
            </View>
        </View>
    )

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(StepOne)