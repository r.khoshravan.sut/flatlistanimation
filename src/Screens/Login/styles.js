import { StyleSheet, Dimensions, I18nManager, Platform } from 'react-native';
import Helper from '../../Share/Helper'
const { width, height } = Dimensions.get('screen');
// const ffamilybold = I18nManager.isRTL ? 'IRANSANS' : 'TwCenMT-Bold';
// const ffamily = I18nManager.isRTL ? 'IRANSANS' : 'TwCenMT-Regular';
// const fontsizes = width < 680 ? [12, 14, 16, 18, 20, 9] : [14, 16, 18, 20, 24, 9];
export default StyleSheet.create({
    ViewContainer: {
        flex: 1
    },
    bottomView: {
        // backgroundColor: 'red',
        position: 'absolute',
        bottom: 0,
        width,
    },
    botomViewinner: {
        paddingVertical: 35,
        paddingHorizontal: '5%'
    },
    textWithe: {
        fontFamily: Helper.ffamily(),
        color: '#FFFF'
    },
    loginTxt: {
        fontSize: Helper.ReturnFont(5)
    },
    botomViewinnerRow: {
        width: '100%', flexDirection: 'row',

    },
    redbuttom: {
        ...Platform.select({
            ios: {
                shadowOffset: { width: 5, height: 5 },
                shadowColor: Helper.ReturnStatusColor('mainRed'),
                shadowOpacity: 0.5,
                shadowRadius: 10,
            },
            android: {
                elevation: 2
            }
        }),
        width: '70%',
        margin: 15,
        borderRadius: 10,
        paddingVertical: 20,
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: Helper.ReturnStatusColor('mainRed')
    },
    redbuttomP2: {
        ...Platform.select({
            ios: {
                shadowOffset: { width: 5, height: 5 },
                shadowColor: Helper.ReturnStatusColor('mainRed'),
                shadowOpacity: 0.5,
                shadowRadius: 10,
            },
            android: {
                elevation: 2
            }
        }),
        width: '93%',
        margin: 15,
        borderRadius: 10,
        paddingVertical: 20,
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: Helper.ReturnStatusColor('mainRed')
    },
    greybuttomP2input: {
        // backgroundColor: 'red',
        width: '80%',
        paddingVertical: 25,
        paddingHorizontal: 15,
        color: '#FFFF',
        fontSize: Helper.ReturnFont(5),
        fontFamily: Helper.ffamily(),
        borderRadius: 10,
    },
    greybuttomP3: {
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                shadowOffset: { width: 5, height: 5 },
                shadowColor: Helper.ReturnStatusColor('mainGrey'),
                shadowOpacity: 0.5,
                shadowRadius: 10,
            },
            android: {
                elevation: 2
            }
        }),
        width: '93%',
        margin: 15,
        borderRadius: 10,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Helper.ReturnStatusColor('mainGrey')
    },
    greybuttomP2: {
        ...Platform.select({
            ios: {
                shadowOffset: { width: 5, height: 5 },
                shadowColor: Helper.ReturnStatusColor('mainGrey'),
                shadowOpacity: 0.5,
                shadowRadius: 10,
            },
            android: {
                elevation: 2
            }
        }),
        width: '93%',
        margin: 15,
        borderRadius: 10,
        paddingVertical: 20,
        justifyContent: 'center',
        paddingHorizontal: 25,
        color: '#FFFF',
        fontSize: Helper.ReturnFont(5),
        fontFamily: Helper.ffamily(),
        backgroundColor: Helper.ReturnStatusColor('mainGrey')
    },
    greybuttomEN: {
        ...Platform.select({
            ios: {
                shadowOffset: { width: 5, height: 5 },
                shadowColor: Helper.ReturnStatusColor('mainRed'),
                shadowOpacity: 0.5,
                shadowRadius: 10,
            },
            android: {
                elevation: 2
            }
        }),
        width: '20%',
        marginVertical: 15,
        borderRadius: 10,
        paddingVertical: 20,
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: Helper.ReturnStatusColor('mainGrey')
    },
    textWelcom: {
        color: Helper.ReturnStatusColor('blueText'),
        fontSize: Helper.ReturnFont(10)
    },
    textWelcom2: {
        color: Helper.ReturnStatusColor('blueText'),
        fontSize: Helper.ReturnFont(2)
    },
    viewWelcomtxt: {
        margin: '3%'
    }

})