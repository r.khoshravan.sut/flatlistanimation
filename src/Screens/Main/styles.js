import { StyleSheet, Dimensions, Platform } from 'react-native';
import Helper from '../../Share/Helper';

export default StyleSheet.create({
    modalvpinfo: {
        width: 30,
        height: 30,
        // top: 20, right: 20,
        // top: 25,
        borderRadius: 15,
        marginTop: 15,
        backgroundColor: 'gray',
        position: 'absolute',
        opacity: 0.5,
        alignSelf: 'center',
    },
    madalRezvpinfo: {
        width: 30,
        height: 30,
        // transform: [{ rotate: '90deg' }],
        justifyContent: 'center',
        alignItems: 'center',
        top: 40,
        right: 40,
        position: 'absolute',
        zIndex: 3,
    },
    modalLoading: {
        justifyContent: 'center', alignItems: 'center',
        flex: 1, width: '100%', height: '100%',
        alignSelf: 'center', backgroundColor: 'black',
        opacity: .5
    },

    MyItemT: {
        width: Helper.MyWidth(.9),
        padding: 10,
        backgroundColor: Helper.ReturnStatusColor('mainGrey'),
        borderRadius: 15,
        backgroundColor: Helper.ReturnStatusColor('sefid'),
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {
                    width: 2,
                    height: 2,
                },
                shadowOpacity: 3,
                // shadowRadius: 2.52,
            },
            android: {
                elevation: 2,
            },
        }),
    },

})