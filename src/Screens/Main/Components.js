import React from 'react';
import {
    View, Text,
    ImageBackground, Image
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { TextR } from '../../Share/ComponentsShare';
import Helpers from '../../Share/Helper';
import styles from './styles'
const Components = {

    renderItem: (props) => {
        const { item, index, onClicItem } = props
        return (
            <TouchableOpacity
                onPress={() => onClicItem({ personeData: item })}
                style={styles.MyItemT}
            >
                <TextR>{item.name}</TextR>
                <TextR >{item.username}</TextR>
                <TextR >{item.email}</TextR>
                <TextR >{item.phone}</TextR>
            </TouchableOpacity>
        )
    },

}
export default Components;