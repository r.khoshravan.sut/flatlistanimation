import React, { useState, useEffect } from 'react';
import {
    View, Text, Image, KeyboardAvoidingView, StatusBar,
    FlatList, I18nManager, ImageBackground, ScrollView, Modal, ActivityIndicator
} from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import stringsoflanguages from '../../Components/stringsoflanguages'
import Header from '../../Components/Header';
import styles from './styles'
import Helpers from '../../Share/Helper';
import { HeaderIconLeftMain, HeaderIconRightMain, HeaderTitle, MyScroll, request, requestGET, TextR } from '../../Share/ComponentsShare';
import Components from './Components';
const Main = (props) => {
    const insets = useSafeAreaInsets();
    const [modalVisible, setmodalVisible] = useState(true)
    const [loading, setloading] = useState(true)
    const [Projects, setProjects] = useState([])
    const [DataModal, setDataModal] = useState(null)

    useEffect(() => {
        getDatas();
    }, []);

    const getDatas = () => {
        requestGET(
            'users',
            () => { },
            () => { },
            (res) => {
                //   if (res.Resoult == 'Success') {
                setProjects(res)
                setmodalVisible(false)
                // if (res !== 'undefined')
                // }
                // console.log(JSON.stringify(res))
            },
            () => { },
        );
    }
    const onClicItem = (props) => {
        setDataModal(props.personeData)
        setloading(false)
        setmodalVisible(true)
    }
    return (
        <View style={{ flex: 1 }}>
            <StatusBar barStyle="light-content" backgroundColor={Helpers.ReturnStatusColor('blueText')} />
            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior="height"
            // keyboardVerticalOffset={100}
            >
                <View style={{
                    paddingTop: insets.top,
                    paddingBottom: insets.bottom,
                    paddingHorizontal: '7%',
                }}>
                    <Header
                        Left={() => HeaderIconLeftMain({ "navigation": props.navigation })}
                        Right={() => HeaderIconRightMain({ "navigation": props.navigation })}
                    />

                </View>
                <View>
                    <MyScroll
                        distanceBetweenItem={5}
                        data={Projects}
                        keyExtractor={(item, index) => `${index}`}
                        renderItem={({ item, index }) => Components.renderItem({ item: item, index: index, onClicItem: onClicItem })}>
                    </MyScroll>
                </View>
            </KeyboardAvoidingView>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
            >{
                    loading ?
                        <View style={styles.modalLoading}>
                            <ActivityIndicator size="large" color="white" />
                            <Text style={{ color: 'white' }}>Loading ...</Text>
                        </View> :
                        DataModal != null &&
                        <View style={{
                            backgroundColor: 'white', width: Helpers.MyWidth(1),
                            height: Helpers.MyHeight(1), justifyContent: 'center', alignItems: 'center'
                        }}>
                            <View style={styles.madalRezvpinfo}>
                                <View style={styles.modalvpinfo} />
                                <IconAntDesign
                                    onPress={() => {
                                        setmodalVisible(!modalVisible);
                                    }}
                                    name={'close'}
                                    type={'AntDesign'}
                                ></IconAntDesign>
                            </View>
                            <TextR>{DataModal.name}</TextR>
                            <TextR>{DataModal.username}</TextR>
                            <TextR>{DataModal.email}</TextR>
                            <TextR>{DataModal.phone}</TextR>
                            <TextR>{DataModal.website}</TextR>
                            <TextR>{DataModal.company?.name}</TextR>
                            <TextR>{DataModal.company?.catchPhrase}</TextR>
                            <TextR>{DataModal.company?.bs}</TextR>
                            <TextR>{DataModal.address?.suite
                                + " " + DataModal.address?.street + " " + DataModal.address?.city
                            }</TextR>
                            <TextR>{DataModal.address?.zipcode}</TextR>
                        </View>
                }
            </Modal>
        </View>
    )

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Main)