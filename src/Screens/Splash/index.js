import React, { useEffect } from 'react'
import { View, Text, Image } from 'react-native'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Helpers from '../../Share/Helper'
const Splash = (props) => {
    const CheckLogin = async () => {
        let Token;
        // try {
        Token = await AsyncStorage.getItem('Token');
        // } catch (e) {
        //     // Restoring token failed
        //     console.log(JSON.stringify(e))
        // }
        // if (Token) {
        //     props.ChengToken(Token)
        props.navigation.replace('MyApp')
        // } else {
        // props.navigation.replace('MyApp')
        // props.navigation.replace('Login')

        // }
        // setLoading(false)
    }
    useEffect(() => {
        setTimeout(() => {
            CheckLogin()
        }, 2000);
    }, [])
    return (
        <View style={{ width: Helpers.MyWidth(1), height: Helpers.MyHeight(1) }}>
            <Image style={{ width: '100%', height: '100%' }}
                resizeMode={'cover'}
                source={require('../../Assets/Img/890451.jpg')}></Image>
        </View>
    )

}

const mapStateToProps = (state) => ({
    userToken: state.Customer.userToken
})

const mapDispatchToProps = (dispatch) => {
    return {
        ChengToken: (userToken) => {
            const action = {
                type: 'CHANGE_C_userToken',
                userToken,
            };
            dispatch(action);
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash)
