import React, { useState } from 'react';
import {
    View, Animated, FlatList,
    Text, Image, TextInput, Dimensions, StyleSheet,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Helpers from './Helper';
import styles from './Styles';
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const { height: wHeight } = Dimensions.get('window');
const BASE_URL = 'https://jsonplaceholder.typicode.com/'
const height = wHeight;

const HeaderIconRightMain = (props) => {
    return (
        <TouchableOpacity
            style={{ justifyContent: 'center' }}
        // onPress={() => props.navigation.toggleDrawer()}
        >
            {/* <MaterialIcons name="chat" size={35} style={{ textAlign: 'left' }} /> */}
            <Image style={{
                width: 30, height: 30,
                marginTop: 5,
            }} resizeMode={'contain'} source={require('../Assets/Icons/messaging.png')} />
        </TouchableOpacity>
    )
}
const HeaderIconLeftMain = (props) => {
    return (
        <TouchableOpacity
            style={{ justifyContent: 'center' }}
            onPress={() => props.navigation.toggleDrawer()}>
            <Entypo name="menu" size={35} style={{ textAlign: 'left' }} />
        </TouchableOpacity>
    )
}
const HeaderTitle = (props) => {
    return (
        <View style={styles.ViewTitle}>
            <Text style={styles.textTitle}>{props.Title}</Text>
            <Text style={styles.UnderTitle}>{props.UnderTitle}</Text>
        </View>
    )
}
const MyScroll = (props) => {

    const y = new Animated.Value(0);
    const onScroll = Animated.event([{ nativeEvent: { contentOffset: { y } } }], {
        useNativeDriver: true,
    });
    return (
        <AnimatedFlatList
            scrollEventThrottle={16}
            bounces={false}
            data={props.data}
            renderItem={(data) => {
                let item = props.renderItem(data);
                const { index } = data;
                return <ScrollItem {...{ index, y, item, distanceBetweenItem: props.distanceBetweenItem }} />;
            }}
            {...{ onScroll }}
            {...props.otherProps}
        />
    );
}
const ScrollItem = ({
    y,
    index,
    distanceBetweenItem,
    item,
}) => {
    const [cardHeight, setCardHeight] = useState(0);
    const position = Animated.subtract(index * cardHeight, y);
    const isDisappearing = -cardHeight;
    const isTop = 0;
    const isBottom = height - cardHeight;
    const isAppearing = height;
    const translateY = Animated.add(
        y,
        y.interpolate({
            inputRange: [0, 0.00001 + index * cardHeight],
            outputRange: [0, -index * cardHeight],
            extrapolateRight: 'clamp',
        })
    );
    const scale = position.interpolate({
        inputRange: [isDisappearing, isTop, isBottom, isAppearing],
        outputRange: [0.85, 1, 1, 1],
        extrapolate: 'clamp',
    });
    const opacity = position.interpolate({
        inputRange: [isDisappearing, isTop, isBottom, isAppearing],
        outputRange: [1, 1, 1, 1],
    });
    return (
        <Animated.View
            style={[
                {
                    marginVertical: distanceBetweenItem,
                    alignSelf: 'center',
                },
                { opacity, transform: [{ translateY }, { scale }] },
            ]}
            key={index}
        >
            <View
                onLayout={(event) => {
                    var { height } = event.nativeEvent.layout;
                    setCardHeight(height + distanceBetweenItem * 2);
                }}
            >
                {item}
            </View>
        </Animated.View>
    );
}
const TextR = (props) => {
    return (<Text style={styles.UnderTitle}>{props.children}</Text>)
}
const request = async (
    method,
    url,
    data,
    startCB,
    suceecRB,
    successCB,
    errorCB,
) => {
    const sendRequest = async () => {
        startCB && startCB();
        // console.log(JSON.stringify(data))
        await fetch(BASE_URL + url, {
            method: method,
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((response) => {
                // console.log(response)
                suceecRB && suceecRB(response);
                return response.json();
            })
            .then((result) => {
                successCB && successCB(result);
            })
            .catch((err) => {
                // console.log(err)
                errorCB && errorCB();
            });
    };
    sendRequest();
    // const isConnected = await checkInternetConnection();
    // if(isConnected){
    //   sendRequest();
    // }else{
    //   store.dispatch(setTempStateRequest(sendRequest))
    // }
};
const requestGET = async (
    url,
    startCB,
    suceecRB,
    successCB,
    errorCB,
) => {
    const sendRequestGET = async () => {
        startCB && startCB();
        // console.log(JSON.stringify(data))
        await fetch(BASE_URL + url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((response) => {
                // console.log(response)
                suceecRB && suceecRB(response);
                return response.json();
            })
            .then((result) => {
                successCB && successCB(result);
            })
            .catch((err) => {
                // console.log(err)
                errorCB && errorCB();
            });
    };
    sendRequestGET();
    // const isConnected = await checkInternetConnection();
    // if(isConnected){
    //   sendRequest();
    // }else{
    //   store.dispatch(setTempStateRequest(sendRequest))
    // }
};
export {
    HeaderIconRightMain,
    HeaderIconLeftMain, TextR,
    HeaderTitle, MyScroll, request, requestGET
}