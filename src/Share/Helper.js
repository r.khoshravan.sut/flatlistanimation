import { Dimensions, I18nManager } from 'react-native';
const { width, height } = Dimensions.get('screen');
const Helpers = {

    ffamilybold: item => { return I18nManager.isRTL ? 'IRANSANS' : 'TwCenMT-Bold' },
    ffamily: item => { return I18nManager.isRTL ? 'IRANSANS' : 'TwCenMT-Regular' },
    MyWidth: item => { return (width * item) },
    MyHeight: item => { return (height * item) },
    ReturnStatusColor: item => {
        switch (item) {
            case 'mainRed':
                return '#ee3328';
            case 'mainGrey':
                return '#2c2c2d';
            case 'blueText':
                return '#536a89';
            case 'lighyGrey':
                return '#B0B0B0';
            case 'processing':
                return '#1fc792';
            case 'on-hold':
                return '#ff9f43';
            case 'completed':
                return '#1fc792';
            case 'cancelled':
                return '#ff4141';
            case 'refunded':
                return '#ff4141';
            case 'failed':
                return '#ff4141';
            case 'sefid':
                return '#FFFF';
        }
    },
    ReturnFont: item => {
        switch (item) {
            case 0:
                return width < 680 ? 9 : 12;
            case 1:
                return width < 680 ? 12 : 14;
            case 2:
                return width < 680 ? 14 : 16;
            case 3:
                return width < 680 ? 16 : 18;
            case 4:
                return width < 680 ? 18 : 20;
            case 5:
                return width < 680 ? 20 : 24;
            case 6:
                return width < 680 ? 24 : 26;
            case 10:
                return width < 680 ? 32 : 34;

        }
    }
}

export default Helpers;