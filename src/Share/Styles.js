import { StyleSheet, Dimensions, Platform } from 'react-native';
import Helper from './Helper';
export default StyleSheet.create({
    UnderTitle: {
        fontFamily: Helper.ffamily(),
        color: Helper.ReturnStatusColor('mainGrey'),
        fontSize: Helper.ReturnFont(4)
    },
})