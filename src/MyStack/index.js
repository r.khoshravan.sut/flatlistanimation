import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { View, Text, Image, KeyboardAvoidingView, StatusBar, } from 'react-native';
import Login from '../Screens/Login/index';
import Main from '../Screens/Main/index';
import Splash from '../Screens/Splash/index';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Stack2 = createStackNavigator();
const MyApp = (p) => {
    return (
        <View style={{ flex: 1 }}>
            <Stack2.Navigator screenOptions={{ headerShown: false }}>
                <Stack2.Screen name="Main" component={Main} />
            </Stack2.Navigator>
        </View>
    )
}
const MyStack = (props) => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="MyApp" component={MyApp} />
            <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
    )
}

// const mapStateToProps = (state) => ({
//     userToken: state.Customer.userToken
// })

// const mapDispatchToProps = (dispatch) => {
//     return {
//         ChengToken: (userToken) => {
//             const action = {
//                 type: 'CHANGE_C_userToken',
//                 userToken,
//             };
//             dispatch(action);
//         },
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(MyStack)
export default MyStack