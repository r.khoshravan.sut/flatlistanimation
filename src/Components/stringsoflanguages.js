import LocalizedStrings from 'react-native-localization';
const strings = new LocalizedStrings({
    en: {
        welcom: 'Welcom to',
        the: 'the',
        CTDev: 'CT Dev',
        Newleveloffeatures: 'New level of features',
        Withnewapp: 'With new app',
        Login: 'Log in',
        NextStep: 'Next Step',
        EnterCodewehaveSent: 'Enter Code we have Sent',
        Enteremailorphone: 'Enter email or phone'
    },
    fa: {
        welcom: 'خوش آمدید به',
        the: '',
        CTDev: 'اپلیکیشن CT Dev',
        Newleveloffeatures: 'جدید ترین امکانات',
        Withnewapp: 'با جدیدترین اپلیکیشن ها',
        Login: 'ورود',
        NextStep: 'مرحله بعد',
        EnterCodewehaveSent: 'کد ارسال شده را وارد کنید',
        Enteremailorphone: 'شماره تلفن یا ایمیل خود را وارد کنید'
    },
})
export default strings;