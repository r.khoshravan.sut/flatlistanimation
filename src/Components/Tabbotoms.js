import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import Helpers from '../Share/Helper';
import { MenuButton } from '../Share/ComponentsShare';
const Tabbotoms = (props) => {
    const [tab, settab] = useState(0);
    const Maintabs = ['Main', 'Services', 'Tracking', 'Profile']
    const onclick = (M) => {
        settab(M)
        props.navigation.replace(Maintabs[M])
    }
    return (
        <View style={{
            alignItems: "center", justifyContent: 'center',
            width: Helpers.MyWidth(1),
            position: 'absolute',
            bottom: 0, zIndex: 50,
            height: 80,
        }}>
            <View style={{
                width: Helpers.MyWidth(.85),
                height: 70,
                borderRadius: 15,
                backgroundColor: 'black',
                flexDirection: 'row'
            }}>
                {MenuButton({
                    onPress: () => onclick(0),
                    source: require('../Assets/Icons/Home.png'),
                    Mtext: 'Home'
                    , matncolor: tab == 0 ? '#FFFF' : Helpers.ReturnStatusColor('mainGrey')
                    , imagcolor: tab == 0 ? Helpers.ReturnStatusColor('mainGrey') : '0'
                })}
                {MenuButton({
                    onPress: () => onclick(1),
                    source: require('../Assets/Icons/75668.png'), Mtext: 'Service'
                    , matncolor: tab == 1 ? '#FFFF' : Helpers.ReturnStatusColor('mainGrey')
                    , imagcolor: tab == 1 ? Helpers.ReturnStatusColor('mainGrey') : '0'
                })}
                {MenuButton({
                    onPress: () => onclick(2),
                    source: require('../Assets/Icons/Tracking.png'), Mtext: 'Tracking'
                    , matncolor: tab == 2 ? '#FFFF' : Helpers.ReturnStatusColor('mainGrey')
                    , imagcolor: tab == 2 ? Helpers.ReturnStatusColor('mainGrey') : '0'
                })}
                {MenuButton({
                    onPress: () => onclick(3),
                    source: require('../Assets/Icons/Profile.png'), Mtext: 'Profile'
                    , matncolor: tab == 3 ? '#FFFF' : Helpers.ReturnStatusColor('mainGrey')
                    , imagcolor: tab == 3 ? Helpers.ReturnStatusColor('mainGrey') : '0'
                })}
            </View>
        </View>
    )
}
export default Tabbotoms