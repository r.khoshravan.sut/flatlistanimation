import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

const Header = (props) => {

    return (
        <View style={styles.wol}>
            <View style={styles.Left}>{props.Left && props.Left()}</View>
            <View style={styles.Center}>{props.Center && props.Center()}</View>
            <View style={styles.Right}>{props.Right && props.Right()}</View>
        </View>
    )

}

const styles = StyleSheet.create({
    wol: {
        marginVertical: 35,
        // backgroundColor: 'red',
        justifyContent: 'space-between', alignItems: 'center',
        width: '100%',
        flexDirection: 'row'
    },
    Right: {
        // backgroundColor: 'green',
        flex: .3,
        // margin: 10,
        direction: 'rtl',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    Center: {
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'blue',
        flex: .4
    },
    Left: {
        // margin: 10,
        justifyContent: 'center',
        // backgroundColor: 'grey',
        flex: .3
    },
});
const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Header)