import React from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { TextR } from '../Share/ComponentsShare'

const CustomDrawerContent = (props) => {

    return (
        <View style={{ width: '100%', height: '100%', background: 'green', alignItems: 'center', justifyContent: 'center' }}>
            <TextR>Thank you for your patience</TextR>
            <TextR>Ramin Khoshravan</TextR>
        </View>
    )

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(CustomDrawerContent)